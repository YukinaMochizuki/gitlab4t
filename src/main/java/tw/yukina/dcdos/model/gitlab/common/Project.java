package tw.yukina.dcdos.model.gitlab.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.net.URL;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Project {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("avatar_url")
    private URL avatarUrl;

    @JsonProperty("web_url")
    private URL webUrl;

    @JsonProperty("namespace")
    private String namespace;

    @JsonProperty("path_with_namespace")
    private String pathWithNamespace;

}
