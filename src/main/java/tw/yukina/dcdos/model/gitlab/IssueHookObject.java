package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonDeserialize(using = JsonDeserializer.None.class)
public class IssueHookObject extends GitLabObject{

    @JsonProperty("object_attributes")
    private Issue issue;

    @JsonProperty("changes")
    private Changes changes;
}
