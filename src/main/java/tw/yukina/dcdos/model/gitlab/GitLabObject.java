package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import tw.yukina.dcdos.model.gitlab.common.Project;
import tw.yukina.dcdos.model.gitlab.common.User;
import tw.yukina.dcdos.model.gitlab.deserializer.GitLabObjectDeserializer;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@JsonDeserialize(using = GitLabObjectDeserializer.class)
public class GitLabObject {

    @JsonProperty("object_kind")
    private ObjectType objectType;

    @JsonProperty("event_type")
    private String eventType;

    @JsonProperty("user")
    private User user;

    @JsonProperty("project_id")
    private int projectId;

    @JsonProperty("project")
    private Project project;
}
