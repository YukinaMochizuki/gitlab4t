package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonValue;

public enum StateType {
    OPENED("opened"),
    CLOSED("closed");

    private final String field;

    StateType(String field) {
        this.field = field;
    }

    @JsonValue
    public String getField(){
        return field;
    }

}
