package tw.yukina.dcdos.model.gitlab.deserializer;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import tw.yukina.dcdos.model.deserializer.AbstractDeserializer;
import tw.yukina.dcdos.model.gitlab.GitLabObject;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;
import tw.yukina.dcdos.model.gitlab.ObjectType;

import java.io.IOException;

public class GitLabObjectDeserializer extends AbstractDeserializer<GitLabObject> {
    @Override
    public GitLabObject deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JacksonException {

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String type = node.get("object_kind").asText();

        addAvailableType(ObjectType.ISSUE.getField(), IssueHookObject.class);


        return typeDeserialize(type, node, jsonParser.getCodec()).orElseThrow(() -> throwTypeNotFound(type, jsonParser));
    }
}
