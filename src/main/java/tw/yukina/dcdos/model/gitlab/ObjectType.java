package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ObjectType {
    ISSUE("issue"),
    NOTE("note"),
    BLOCK("block");

    private final String field;

    ObjectType(String field) {
        this.field = field;
    }

    @JsonValue
    public String getField(){
        return field;
    }

}
