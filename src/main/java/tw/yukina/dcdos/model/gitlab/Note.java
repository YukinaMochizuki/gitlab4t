package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.net.URL;
import java.time.ZonedDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Note {

    @JsonProperty("author_id")
    private int author_id;

    @JsonProperty("created_at")
    private ZonedDateTime createdAt;

    @JsonProperty("id")
    private int id;

    @JsonProperty("note")
    private String note;

    @JsonProperty("updated_at")
    private ZonedDateTime updatedAt;

    @JsonProperty("updated_by_id")
    private int updatedById;

    @JsonProperty("url")
    private URL url;
}
