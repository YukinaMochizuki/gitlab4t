package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ActionType {
    OPEN("open"),
    CLOSE("close"),
    UPDATE("update"),
    REOPEN("reopen");

    private final String field;

    ActionType(String field) {
        this.field = field;
    }

    @JsonValue
    public String getField(){
        return field;
    }

}
