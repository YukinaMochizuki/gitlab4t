package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import tw.yukina.dcdos.model.gitlab.common.Label;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LabelChanges {

    @JsonProperty("previous")
    private List<Label> previousLabels;

    @JsonProperty("current")
    private List<Label> currentLabels;

}
