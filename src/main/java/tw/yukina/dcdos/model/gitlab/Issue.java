package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import tw.yukina.dcdos.model.gitlab.common.Label;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Issue {

    @JsonProperty("author_id")
    private int author_id;

    @JsonProperty("closed_at")
    private ZonedDateTime closedAt;

    @JsonProperty("created_at")
    private ZonedDateTime createdAt;

    @JsonProperty("due_date")
    private Date dueDate;

    @JsonProperty("description")
    private String description;

    @JsonProperty("iid")
    private int issueId;

    @JsonProperty("last_edited_at")
    private ZonedDateTime lastEditedAt;

    @JsonProperty("last_edited_by_id")
    private int lastEditedById;

    @JsonProperty("updated_at")
    private ZonedDateTime updatedAt;

    @JsonProperty("updated_by_id")
    private int updatedById;

    @JsonProperty("milestone_id")
    private int milestoneId;

    @JsonProperty("title")
    private String title;

    @JsonProperty("url")
    private URL url;

    @JsonProperty("assignee_ids")
    private List<Integer> assigneeIds;

    @JsonProperty("labels")
    private List<Label> labels = new ArrayList<>();

    @JsonProperty("state")
    private StateType state;

    @JsonProperty("action")
    private ActionType action;
}
