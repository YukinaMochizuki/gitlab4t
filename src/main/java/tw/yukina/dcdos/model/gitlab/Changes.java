package tw.yukina.dcdos.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Changes {

    @JsonProperty("labels")
    private LabelChanges labelChanges;

}
