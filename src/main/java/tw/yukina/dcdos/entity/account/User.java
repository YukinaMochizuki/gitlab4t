package tw.yukina.dcdos.entity.account;

import lombok.*;
import tw.yukina.dcdos.constants.Role;
import tw.yukina.dcdos.entity.AbstractEntity;
import tw.yukina.dcdos.manager.notification.UserIssueNotifyType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    @Column(unique = true)
    private Long telegramUserId;

    @Column(unique = true)
    private Integer gitLabUserId;

    @NotNull
    private Role role;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<UserIssueNotifyType> enableNotificationPlan = new HashSet<>();

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "user")
    private UserCache userCache;

    public String getTelegramUserName(){
        if(userCache == null)return "";
        String tgUsername = userCache.getTelegramUserName();
        return tgUsername != null ? "@" + userCache.getTelegramUserName() : "";
    }

    public String getDisplayName(){
        if(userCache == null)return "";
        String display = userCache.getDisplayName();
        return display != null ? display : "";
    }
}
