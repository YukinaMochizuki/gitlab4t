package tw.yukina.dcdos.command.system.notification;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import picocli.CommandLine;
import tw.yukina.dcdos.command.AbstractAssistantCommand;
import tw.yukina.dcdos.constants.Role;
import tw.yukina.dcdos.manager.NotificationManager;
import tw.yukina.dcdos.repository.UserRepository;


@Component
@CommandLine.Command(name = "/notify", subcommands = {CommandLine.HelpCommand.class, addNotify.class, refreshNotify.class})
@Getter
public class NotificationCommand extends AbstractAssistantCommand {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationManager notificationManager;

    @Override
    public String getCommandName() {
        return "notify";
    }

    @Override
    public Role[] getPermissions() {
        return new Role[]{Role.MEMBER};
    }
}
