package tw.yukina.dcdos.command.system.notification;

import picocli.CommandLine;
import tw.yukina.dcdos.entity.account.User;
import tw.yukina.dcdos.manager.notification.UserIssueNotifyType;

@CommandLine.Command(name = "add", description = "新增通知類型到這個使用者")
public class addNotify extends AbstractNotifySubCommand implements Runnable{

    @CommandLine.Parameters(index = "0", paramLabel = "<type>", description = "Valid values: ${COMPLETION-CANDIDATES}")
    private UserIssueNotifyType userIssueNotifyType;

    @Override
    public void run() {
        User user = parentCommand.getUserRepository().findByTelegramUserId(parentCommand.getChatId());
        if(user == null){
            parentCommand.sendMessageToChatId("您尚未註冊為用戶");
        } else {
            user.getEnableNotificationPlan().add(userIssueNotifyType);
            parentCommand.getUserRepository().save(user);
        }
        parentCommand.getNotificationManager().refreshNotifyPlan();
    }
}
