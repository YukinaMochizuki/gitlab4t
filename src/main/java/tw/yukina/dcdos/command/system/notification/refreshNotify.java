package tw.yukina.dcdos.command.system.notification;

import picocli.CommandLine;

@CommandLine.Command(name = "refresh", description = "重新整理已有的通知計畫")
public class refreshNotify extends AbstractNotifySubCommand implements Runnable{
    @Override
    public void run() {
        parentCommand.getNotificationManager().refreshNotifyPlan();
    }
}
