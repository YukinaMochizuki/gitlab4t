package tw.yukina.dcdos.command.system.notification;

import lombok.Getter;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;

@Command
@Getter
public abstract class AbstractNotifySubCommand {
    @Option(names = {"-h", "--help"}, usageHelp = true, description = "display this help message")
    public boolean usageHelpRequested;

    @ParentCommand
    public NotificationCommand parentCommand;
}
