package tw.yukina.dcdos.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.IssuesApi;
import org.gitlab4j.api.NotesApi;
import org.gitlab4j.api.models.Project;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tw.yukina.dcdos.model.deserializer.ZonedDateTimeDeserializer;

import java.time.ZonedDateTime;

@Configuration
public class GitLabConfig {

    @Value("${gitlab.token}")
    private String gitLabToken;

    @Value("${gitlab.namespace}")
    private String namespace;

    @Value("${gitlab.project}")
    private String project;

    @Bean
    public GitLabApi getGitLabApi(){
        return new GitLabApi("https://gitlab.com/", gitLabToken);
    }

    @Bean
    public IssuesApi getIssuesApi(GitLabApi gitLabApi){
        return gitLabApi.getIssuesApi();
    }

    @Bean
    public NotesApi getNoteApi(GitLabApi gitLabApi){
        return gitLabApi.getNotesApi();
    }

    @Bean
    public Project getProject(GitLabApi gitLabApi) throws GitLabApiException {
        return gitLabApi.getProjectApi().getProject(namespace, project);
    }

    @Bean
    public ObjectMapper getObjectMapper(){
        SimpleModule objectMapperModule = new SimpleModule();
        objectMapperModule.addDeserializer(ZonedDateTime.class, new ZonedDateTimeDeserializer());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(objectMapperModule);
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return objectMapper;
    }
}