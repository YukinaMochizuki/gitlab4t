package tw.yukina.dcdos.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tw.yukina.dcdos.manager.NotificationManager;
import tw.yukina.dcdos.model.gitlab.GitLabObject;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;
import tw.yukina.dcdos.model.gitlab.NoteHookObject;

@RestController
public class Webhook {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private NotificationManager notificationManager;

    @PostMapping("/gitlab/issue/update")
    void issueUpdate(@RequestBody JsonNode responseJsonNode){
        try {
            notificationManager.notification(objectMapper.treeToValue(responseJsonNode, IssueHookObject.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/gitlab/issue/note")
    void issueNote(@RequestBody JsonNode responseJsonNode){
        try {
            System.out.println(objectMapper.treeToValue(responseJsonNode, NoteHookObject.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
