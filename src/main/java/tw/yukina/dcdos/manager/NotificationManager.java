package tw.yukina.dcdos.manager;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import tw.yukina.dcdos.config.TelegramConfig;
import tw.yukina.dcdos.entity.account.User;
import tw.yukina.dcdos.manager.notification.Filter;
import tw.yukina.dcdos.manager.notification.Notification;
import tw.yukina.dcdos.manager.notification.UserIssueNotifyType;
import tw.yukina.dcdos.manager.notification.filter.IssueLabelFilter;
import tw.yukina.dcdos.manager.notification.filter.StateFilter;
import tw.yukina.dcdos.manager.notification.plan.TelegramIssuePlan;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;
import tw.yukina.dcdos.model.gitlab.NoteHookObject;
import tw.yukina.dcdos.model.gitlab.StateType;
import tw.yukina.dcdos.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationManager {

    @Autowired
    private TelegramConfig telegramConfig;

    @Autowired
    private UserRepository userRepository;

    private final List<TelegramIssuePlan> telegramIssuePlans = new ArrayList<>();

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        refreshNotifyPlan();
    }

    public void notification(IssueHookObject issueHookObject){
        Notification notification = new Notification();
        for(TelegramIssuePlan telegramIssuePlan: telegramIssuePlans){
            boolean flag = true;
            for(Filter<IssueHookObject> filter: telegramIssuePlan.getFilters()){
                if(!filter.doFilter(issueHookObject)){
                    flag = false;
                    break;
                }
            }
            if(flag) {
                notification.setTelegramConfig(telegramIssuePlan.getTelegramConfig());
                notification.setRecive(telegramIssuePlan.getReceive());
                notification.telegramIssuePlanList.add(telegramIssuePlan);
            }
        }
        if (notification.telegramIssuePlanList.size() == 0) return;

        notification.sendNotification(issueHookObject);
    }

    public void notification(NoteHookObject noteHookObject){

    }

    public void refreshNotifyPlan(){
        refreshTelegramIssuePlan();
    }

    private void refreshTelegramIssuePlan(){
        telegramIssuePlans.clear();

        List<User> users = userRepository.findAll();
        for(User user: users){
            for(UserIssueNotifyType userIssueNotifyType : user.getEnableNotificationPlan()){
                switch (userIssueNotifyType){
                    case TAGGED_IN_DESCRIPTION_WHEN_ISSUE_OPEN:
                        telegramIssuePlans.add(TelegramIssuePlan.getTaggedInDescriptionWhenOpen(user, telegramConfig));
                        break;
                    case ASSIGNEE_WHEN_ISSUE_OPEN:
                        telegramIssuePlans.add(TelegramIssuePlan.getAssigneeWhenIssueOpenPlanBuilder(user, telegramConfig));
                        break;
                }
            }
        }
    }

    private @NotNull List<Filter<IssueHookObject>> testFilter(){
        List<Filter<IssueHookObject>> list = new ArrayList<>();
        list.add(new StateFilter(StateType.CLOSED));
        list.add(new IssueLabelFilter("To Do"));
        return list;
    }
}
