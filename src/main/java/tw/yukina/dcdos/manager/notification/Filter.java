package tw.yukina.dcdos.manager.notification;

public interface Filter<T> {

    boolean doFilter(T object);

}
