package tw.yukina.dcdos.manager.notification;

import tw.yukina.dcdos.entity.account.Group;

public abstract class AbstractGroupNotificationPlan<T> extends AbstractNotificationPlan<T>{

    public abstract void setReceive(Group group);

}
