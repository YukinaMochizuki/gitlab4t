package tw.yukina.dcdos.manager.notification;

import com.fasterxml.jackson.annotation.JsonValue;

public enum UserIssueNotifyType {
    ASSIGNEE_WHEN_ISSUE_OPEN("assigneeWhenIssueOpen"),
    TAGGED_IN_DESCRIPTION_WHEN_ISSUE_OPEN("taggedInDescriptionWhenIssueOpen");

    private final String field;

    UserIssueNotifyType(String field) {
        this.field = field;
    }

    @JsonValue
    public String getField(){
        return field;
    }
}
