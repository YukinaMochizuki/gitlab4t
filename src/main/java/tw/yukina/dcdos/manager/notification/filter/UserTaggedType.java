package tw.yukina.dcdos.manager.notification.filter;

import com.fasterxml.jackson.annotation.JsonValue;

public enum UserTaggedType {
    ASSIGNEE("assignee"),
    TAGGED("tagged");

    private final String field;

    UserTaggedType(String field) {
        this.field = field;
    }

    @JsonValue
    public String getField(){
        return field;
    }
}
