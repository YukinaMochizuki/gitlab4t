package tw.yukina.dcdos.manager.notification;

import tw.yukina.dcdos.entity.account.User;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;

public abstract class AbstractUserNotificationPlan<T> extends AbstractNotificationPlan<T>{

    public abstract void setReceive(User user);

    public abstract UserIssueNotifyType getUserIssueNotifyType();

    public abstract void notify(IssueHookObject issueHookObject);
}
