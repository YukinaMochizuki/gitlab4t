package tw.yukina.dcdos.manager.notification.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import tw.yukina.dcdos.entity.account.User;
import tw.yukina.dcdos.manager.notification.Filter;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;

@Getter
@AllArgsConstructor
public class IssueUserTagged implements Filter<IssueHookObject> {

    private final User user;
    private final UserTaggedType userTaggedType;

    @Override
    public boolean doFilter(IssueHookObject object) {

        if(userTaggedType.equals(UserTaggedType.ASSIGNEE) && user.getGitLabUserId() != null){
            return object.getIssue().getAssigneeIds().stream().anyMatch(integer -> integer.equals(user.getGitLabUserId()));
        } else if(userTaggedType.equals(UserTaggedType.TAGGED) && user.getUserCache() != null && user.getUserCache().getGitLabUserName() != null){
            return object.getIssue().getDescription().contains(user.getUserCache().getGitLabUserName());
        }

        return false;
    }
}
