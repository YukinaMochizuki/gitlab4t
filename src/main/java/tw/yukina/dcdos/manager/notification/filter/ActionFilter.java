package tw.yukina.dcdos.manager.notification.filter;

import org.jetbrains.annotations.NotNull;
import tw.yukina.dcdos.manager.notification.Filter;
import tw.yukina.dcdos.model.gitlab.ActionType;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;

import java.util.Arrays;

public class ActionFilter implements Filter<IssueHookObject> {

    private final ActionType[] actionType;

    public ActionFilter(ActionType... actionType){
        this.actionType = actionType;
    }

    @Override
    public boolean doFilter(@NotNull IssueHookObject object) {
        return Arrays.stream(actionType).anyMatch(type -> type.equals(object.getIssue().getAction()));
    }
}
