package tw.yukina.dcdos.manager.notification.filter;

import org.jetbrains.annotations.NotNull;
import tw.yukina.dcdos.manager.notification.Filter;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;
import tw.yukina.dcdos.model.gitlab.StateType;

public class StateFilter implements Filter<IssueHookObject> {

    private final StateType stateType;

    public StateFilter(StateType stateType){
        this.stateType = stateType;
    }

    @Override
    public boolean doFilter(@NotNull IssueHookObject object) {
        return object.getIssue().getState().equals(stateType);
    }
}
