package tw.yukina.dcdos.manager.notification.filter;

import org.jetbrains.annotations.NotNull;
import tw.yukina.dcdos.manager.notification.Filter;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;
import tw.yukina.dcdos.model.gitlab.common.Label;

import java.util.List;

public class IssueLabelFilter implements Filter<IssueHookObject> {

    private final String labelsTitle;

    public IssueLabelFilter(@NotNull String labelsTitle) {
        this.labelsTitle = labelsTitle;
    }

    @Override
    public boolean doFilter(@NotNull IssueHookObject object) {
        List<Label> labels = object.getIssue().getLabels();
        return labels.stream().anyMatch(label -> label.getTitle().equals(labelsTitle));
    }
}
