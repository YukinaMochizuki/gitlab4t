package tw.yukina.dcdos.manager.notification;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import tw.yukina.dcdos.config.TelegramConfig;
import tw.yukina.dcdos.entity.account.User;
import tw.yukina.dcdos.manager.notification.plan.TelegramIssuePlan;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;

import java.util.List;

public class Notification {
    private TelegramConfig telegramConfig;
    private User receive;

    public List<TelegramIssuePlan> telegramIssuePlanList;

    public void sendNotification(IssueHookObject issueHookObject) {
        StringBuilder stringBuilder = new StringBuilder();

        for (TelegramIssuePlan telegramIssuePlan: this.telegramIssuePlanList) {
            stringBuilder.append(telegramIssuePlan.getReason()).append("\n");
        }
        stringBuilder.append(issueHookObject.getIssue().getTitle());

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(receive.getTelegramUserId().toString());
        sendMessage.setText(stringBuilder.toString());

        this.telegramConfig.sendMessage(sendMessage);
    }

    public void setTelegramConfig(TelegramConfig telegramConfig) {
        this.telegramConfig = telegramConfig;
    }

    public void setRecive(User receive) {
        this.receive = receive;
    }
}
