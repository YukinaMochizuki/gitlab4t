package tw.yukina.dcdos.manager.notification;

import java.util.List;

public abstract class AbstractNotificationPlan<T> {

    public abstract void ready();

    public abstract List<Filter<T>> getFilters();

    public abstract void notify(T t);

}
