package tw.yukina.dcdos.manager.notification.plan;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import tw.yukina.dcdos.config.TelegramConfig;
import tw.yukina.dcdos.entity.account.User;
import tw.yukina.dcdos.manager.notification.AbstractUserNotificationPlan;
import tw.yukina.dcdos.manager.notification.Filter;
import tw.yukina.dcdos.manager.notification.UserIssueNotifyType;
import tw.yukina.dcdos.manager.notification.filter.ActionFilter;
import tw.yukina.dcdos.manager.notification.filter.IssueUserTagged;
import tw.yukina.dcdos.manager.notification.filter.StateFilter;
import tw.yukina.dcdos.manager.notification.filter.UserTaggedType;
import tw.yukina.dcdos.model.gitlab.ActionType;
import tw.yukina.dcdos.model.gitlab.IssueHookObject;
import tw.yukina.dcdos.model.gitlab.StateType;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class TelegramIssuePlan extends AbstractUserNotificationPlan<IssueHookObject> {

    private User receive;
    private String reason;
    private UserIssueNotifyType userIssueNotifyType;
    private TelegramConfig telegramConfig;
    private List<Filter<IssueHookObject>> filters;

    @Override
    public void ready() {

    }

    @Override
    public void notify(IssueHookObject issueHookObject) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(reason).append("\n").append(issueHookObject.getIssue().getTitle());

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(receive.getTelegramUserId().toString());
        sendMessage.setText(stringBuilder.toString());

        telegramConfig.sendMessage(sendMessage);
    }

    public static TelegramIssuePlan getAssigneeWhenIssueOpenPlanBuilder(User receive, TelegramConfig telegramConfig){
        List<Filter<IssueHookObject>> filters = new ArrayList<>();
        filters.add(new StateFilter(StateType.OPENED));
        filters.add(new IssueUserTagged(receive, UserTaggedType.ASSIGNEE));
        filters.add(new ActionFilter(ActionType.OPEN));

        TelegramIssuePlanBuilder builder = TelegramIssuePlan.builder();
        builder.reason("被指派通知")
                .userIssueNotifyType(UserIssueNotifyType.ASSIGNEE_WHEN_ISSUE_OPEN)
                .filters(filters)
                .receive(receive)
                .telegramConfig(telegramConfig);
        return builder.build();
    }

    public static TelegramIssuePlan getTaggedInDescriptionWhenOpen(User receive, TelegramConfig telegramConfig){
        List<Filter<IssueHookObject>> filters = new ArrayList<>();
        filters.add(new StateFilter(StateType.OPENED));
        filters.add(new IssueUserTagged(receive, UserTaggedType.TAGGED));
        filters.add(new ActionFilter(ActionType.OPEN));

        TelegramIssuePlanBuilder builder = TelegramIssuePlan.builder();
        builder.reason("被 tag 惹")
                .userIssueNotifyType(UserIssueNotifyType.TAGGED_IN_DESCRIPTION_WHEN_ISSUE_OPEN)
                .filters(filters)
                .receive(receive)
                .telegramConfig(telegramConfig);
        return builder.build();
    }
}
