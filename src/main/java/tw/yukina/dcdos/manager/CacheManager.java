package tw.yukina.dcdos.manager;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tw.yukina.dcdos.entity.account.User;
import tw.yukina.dcdos.entity.account.UserCache;
import tw.yukina.dcdos.repository.UserCacheRepository;
import tw.yukina.dcdos.repository.UserRepository;

@Service
public class CacheManager {

    @Autowired
    private UserCacheRepository userCacheRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GitLabApi gitLabApi;

    public void updateGitLabCache(String name) throws GitLabApiException {
        User user = userRepository.findByName(name);
        if(user != null && user.getGitLabUserId() != null){
            UserCache userCache = userCacheRepository.findByUser(user);
            org.gitlab4j.api.models.User gitLabUser = gitLabApi.getUserApi().getUser(user.getGitLabUserId());

            if(userCache == null){
                userCache = new UserCache();
                userCache.setUser(user);
            }
            userCache.setGitLabUserName(gitLabUser.getUsername());
            userCacheRepository.save(userCache);

            System.out.println("cache save!");
            System.out.println(userCache);
        }
    }


}
