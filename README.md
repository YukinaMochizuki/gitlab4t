# GitLabWatcher (fork from [DCDos](https://github.com/YukinaMochizuki/DCDos))
## Table of Contents
- [How to contribute](#how-to-contribute)
- [Technologies Used](#technologies-used)
- [Build](#build)
- [Usage](#usage)
- [Roadmap](#roadmap)
- [Some implementation details](#some-implementation-details)

## How to contribute
Before commit [99d116ef](https://gitlab.com/YukinaMochizuki/gitlab4t/-/commit/99d116ef317e09449d3732a4511a3a23bb00ea56), most of the code was the product of a short hackathon to confirm the viability of an idea.

In subsequent development, we follow the [AngularJS Git Commit Message Conventions](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#heading=h.uyo6cb12dt6w) in writing the commit message. 

### Step 1


## Technologies used
### Development environment
- AdoptOpenJDK 11 with HotSpot JVM
- Software Development Kit Manager: SDKMAN!
- Build System: Gradle 6.8.3
- Database: MariaDB
- CI/CD:
   - [gitlab-ci.yml](https://github.com/YukinaMochizuki/DCDos/blob/master/.gitlab-ci.yml) - This GitHub repository is actually a mirror from my GitLab
   - [Dockerfile](https://github.com/YukinaMochizuki/DCDos/blob/master/Dockerfile) - The gitlab-ci will package the application into an image

### Dependencies
- Spring Boot 2.4.5
  - Spring Boot Starter Web
  - Spring Boot Starter Security
  - Spring Boot Starter Data JPA
- [JSch](http://www.jcraft.com/jsch/) - A pure Java implementation of SSH2. Used to control the unofficial API server
- [Apache Commons Validator](https://commons.apache.org/proper/commons-validator/) - A powerful and customized data verification framework. Used to verify whether the request sent to the API server conforms to the format
- [Picocli](https://picocli.info) - Used to build rich command line applications then connect to communication software
- [TelegramBots](https://github.com/rubenlagus/TelegramBots) - Java library to create bots using Telegram Bots API
- [Project Lombok](https://projectlombok.org) - Getter, Setter, ToString, AllArgsConstructor and more..
- [gitlab4j](https://github.com/gitlab4j/gitlab4j-api) - Planned to be used to create issues

## Setup
### Build
#### To Jar
```Shell
git clone https://github.com/YukinaMochizuki/DCDos.git
./gradlew assemble
```

#### To Docker image
After `gradlew assemble`
```Shell
docker build --tag dcdos .
```

### Configuration
#### application.properties
Put it in `src/main/resources`, or in `/app/config` in the container.

```properties
dcdos.debug=false
telegram.username=Your_Bot_Username
telegram.token=12345:ABCDEF
telegram.permission.master=123456789
```

### Deploy
You may need to modify these commands for your own environment.

#### In my case
```Shell
docker pull registry.lan.yukina.tw/shuvi/dcdos:latest
docker start dcdos-notion-api
docker run -d \
   -v /path/DCDos/config:/app/config \
   -v /path/DCDos/ssh:/app/ssh \
   --name dcdos \
   registry.lan.yukina.tw/shuvi/dcdos:latest
```

## Usage
### Command
In theory, most functions of [Picocli](https://picocli.info) are supported. The implementation of the execution command is in the [`TelegramManager`](https://github.com/YukinaMochizuki/DCDos/blob/master/src/main/java/tw/yukina/dcdos/manager/telegram/TelegramManager.java). You can adjust according to your needs.

```java
@Component
public class TelegramManager {
   public void messageInput(Update update){
   
   //...other code
   
   // find all of the command, assistantCommands is initialized at constructor injection
      Optional<AbstractAssistantCommand> assistantCommandOptional =
         assistantCommands.stream()
         .filter(command -> parameter.get(0).equals(command.getCommandName())).findAny();

   
   //...other code
   
   // execute command
      if(assistantCommandOptional.isPresent()) {
         new CommandLine(assistantCommand).setOut(writer).setErr(writer)
            .setCaseInsensitiveEnumValuesAllowed(true)
            .setUsageHelpWidth(100).execute(args);
      }
   }
}

```
[Full code](https://github.com/YukinaMochizuki/DCDos/blob/master/src/main/java/tw/yukina/dcdos/manager/telegram/TelegramManager.java)


So you can simply extend [`AbstractAssistantCommand`](https://github.com/YukinaMochizuki/DCDos/blob/master/src/main/java/tw/yukina/dcdos/command/AbstractAssistantCommand.java) and implements [`Runnable`](https://docs.oracle.com/javase/8/docs/api/java/lang/Runnable.html) to create a command.

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import picocli.CommandLine.Command;
import tw.yukina.dcdos.command.AbstractAssistantCommand;
import tw.yukina.dcdos.constants.Role;
import tw.yukina.dcdos.manager.telegram.TelegramUserInfoManager;

@Component
@Command(name = "start", description = "Say Hello")
public class Start extends AbstractAssistantCommand implements Runnable {

    @Override
    public void run() {
        sendMessageToChatId("Your telegram user id is " + getChatId());
        sendMessageToChatId("If you want to get some help, please send /help for me");
    }

    @Override
    public String getCommandName() {
        return "start";
    }

    @Override
    public Role[] getPermissions() {
        return new Role[]{Role.GUEST};
    }
}
```

<img src="https://user-images.githubusercontent.com/26710554/124572937-a6759700-de7b-11eb-8935-9686ebcbeb8b.png" alt="drawing" width="400"/>


### Program
Just like creating a command, you can create a program by extends [`AbstractProgramCode`](https://github.com/YukinaMochizuki/DCDos/blob/master/src/main/java/tw/yukina/dcdos/program/AbstractProgramCode.java).

```java
package tw.yukina.dcdos.program.test;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import tw.yukina.dcdos.program.AbstractProgramCode;
import tw.yukina.dcdos.util.ReplyKeyboard;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestProgram extends AbstractProgramCode {

    @Override
    public String[] getKeyword() {
        return new String[]{"測試軟體1"};
    }

    @Override
    public void run() {
        stdout("HelloWorld");
        stdout(getInput());
        stdout("HelloWorld after HelloWorld2");
    }
}
```
Notice `getInput()` is synchronous, so blocking occurs in `stdout(programController.getInput());`.

<img src="https://user-images.githubusercontent.com/26710554/124372521-05d97880-dcbd-11eb-9141-0dd32c5b076f.png" alt="drawing" width="300"/>

#### Edit sent message
Only need to provide the id when sending the message, and then you can use that id to edit the message.

```java
 @Override
 public void run() {
     stdout("testMessage", "uuid1");
     stdout("testMessage2", "uuid2");

     try {
         Thread.sleep(1000);
         stdout("after");
         updateStdout("after edit", "uuid1");
         updateStdout("after edit2", "uuid2");
     } catch (InterruptedException e) {
         e.printStackTrace();
     }
 }
```
[Full code](https://github.com/YukinaMochizuki/DCDos/blob/master/src/main/java/tw/yukina/dcdos/program/test/TestEditMessage.java)

<img src="https://i.imgur.com/iRsE48f.gif" alt="drawing" width="300"/>

## Roadmap


## Some implementation details



